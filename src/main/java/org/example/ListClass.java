package org.example;



import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

// on ouvre la class
public class ListClass {
   static List<String> maList =  new ArrayList<>();
Logger logger = Logger.getLogger(ListClass.class.getName());
    public ListClass() {
        this.maList = getMaList();
        this.logger = getLogger();
    }

   public void ajoutAlaList(Integer index,String ajoutValeur) {
             if(index >= 0 && index <= maList.size()){
             maList.add(index,ajoutValeur);
   }else{logger.warning("Index hors limite"); }}


    public void modifierList(Integer index, String modifValeur){
             if (maList.size()>= 1) {
                 maList.set(index, modifValeur);
             }else{logger.warning("Index hors des limites."); }
       }


    public void listing() {

        if(!maList.isEmpty()) {
                for (String elem :maList) {
                 System.out.println(elem);
            }
        }else{
            logger.warning("Liste vide");}
    }



   public void setMaList(List<String> maList) {
      this.maList = maList;
   }

   public List<String> getMaList() {
      return maList;
   }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }
}
