package org.example;

import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

public class Main {
    static Scanner scan = new Scanner(System.in);
    static Integer index1;
    static Integer index2;
    static Logger logger=Logger.getLogger(Main.class.getName());
    public static void main(String[] args) {
        ListClass listClassInstance = new ListClass();
        Matrice matriceInstance = new Matrice(index1,index2);
        JustePrix justePrixInstance = new JustePrix();




        listClassInstance.ajoutAlaList(0,"Main : index 0 'methode ajoutAlaList' Ajout par instance");
        listClassInstance.ajoutAlaList(1,"Main : index 1 'methode ajoutAlaList' Ajout par instance");
        listClassInstance.ajoutAlaList(2,"Main : index 2 'methode ajoutAlaList' Ajout par instance");
        listClassInstance.listing();
        listClassInstance.modifierList(1,"Main : index 1 'methode modifierList' Modification par instance");
        listClassInstance.listing();

        listClassInstance.listing();

        matriceInstance.matriceBase();
        matriceInstance.matriceView();
        System.out.println("fini matrice");
        justePrixInstance.menu();
        System.out.println("fini justeprix");
    }
}